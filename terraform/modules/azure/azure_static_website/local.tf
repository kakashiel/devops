locals {
  storage_account_name_website    = "${replace("${lower(("${var.resource_group_name}${var.environment}website"))}", "/[^a-z0-9]/","")}"

    domain_static_website = "${replace(azurerm_storage_account.static_website.primary_web_endpoint,"/(?i)https://([^/]+).*/*/","$1")}"
}

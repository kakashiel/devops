# Azure static web site

## Store static content in Azure

This terraform module deploys a storage account link with a DNS, in Azure.

Installs following resources
- Storage account
- CDN profile
- CDN endoint

After  add the endoint on your DNS and add a custome domaine on the CDN profile

## Usage

```hcl

resource "azurerm_resource_group" "image_resizer" {
  name     = "image-resizer-func-rg"
  location = "westeurope"
}

module "azure_static_website" {
  source              = "../../../terraform_modules/azure_static_website"
  resource_group_name = "virtual-bed"
  location            = "Australia East"
  environment         = "lab"
  
  tags = {
    project = "virtual-bed"
  }
}

```

## Inputs

### resource_group_name
The resource group where the resources should be created.

### location
The azure datacenter location where the resources should be created.

### tags
A map of tags to add to all resources
Defaults to "westeurope"
 
### tags
A map of tags to add to all resources. Release and Environment will be auto tagged. 

### environment
The environment where the infrastructure is deployed.

## Outputs

### identity
The MSI identities set on the function app. Returns a list of identities.
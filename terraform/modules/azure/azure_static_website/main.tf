#For static content
resource "azurerm_storage_account" "static_website" {
  account_replication_type  = "RAGRS"
  account_tier              = "Standard"
  account_kind              = "StorageV2"
  location                  = var.location
  name                      = local.storage_account_name_website
  resource_group_name       = var.resource_group_name
  enable_https_traffic_only = true

  static_website {
    index_document          = "index.html"
    error_404_document      = "index.html"
  }
  tags = merge(var.tags, map("environment", var.environment))

}

#CDN
resource "azurerm_cdn_profile" "static_website" {
  name                = "${var.resource_group_name}-cdn"
  location            = var.location
  resource_group_name = var.resource_group_name
  sku                 = var.sku
  tags = merge(var.tags, map("environment", var.environment))

}

resource "azurerm_cdn_endpoint" "static_website" {
  name                          = "${var.resource_group_name}-endoint"
  profile_name                  = "${var.resource_group_name}-cdn"
  location                      = var.location
  resource_group_name           = var.resource_group_name
  querystring_caching_behaviour = "NotSet"

  origin_host_header            = local.domain_static_website
  origin {
    name       = "${var.resource_group_name}origin1"
    host_name  = local.domain_static_website
    http_port  = 80
    https_port = 443
  }
  tags = merge(var.tags, map("environment", var.environment))
}


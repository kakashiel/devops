variable "resource_group_name" {
  description = "The name of the Resource Group the Function App will be contained in."
  default = "App"
}
variable "location" {
  description = "The name of the Resource Group the Function App will be contained in."
  default = "Australia East"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map
  default = {}
}

variable "environment" {
  default     = "lab"
  description = "The environment where the infrastructure is deployed."
}

variable "sku" {
  default     = "Standard_Verizon"
  description = "CDN provider use for the app"
}

locals {
    project_env          = "${var.context.project_name}-${var.context.environment}"
    AZURE_PORTAL_IPS     = "104.42.195.92,40.76.54.131,52.176.6.30,52.169.50.45,52.187.184.26"
    AZURE_DATACENTER_IPS = "0.0.0.0"
    ENABLED_IPS          = var.enable_azdatacenter_access ? "${local.AZURE_PORTAL_IPS},${local.AZURE_DATACENTER_IPS}" : "${local.AZURE_PORTAL_IPS}"
    CALCULATED_NAME      = var.name == "" ? "${local.project_env}-cosmos" : var.name
}
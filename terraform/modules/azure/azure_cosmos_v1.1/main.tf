# *** See https://github.com/hashicorp/terraform/issues/22544 for information on prevent_destroy
locals {
  _offer_type         = "Standard"
  _ip_range_filter    = var.is_virtual_network_filter_enabled ? local.ENABLED_IPS : null
  #When there's no subnet specified, decides whether access from Azure Datacenters is enabled or not
  #Access from Azure portal is always enabled.
  _is_virtual_network_filter_enabled = var.is_virtual_network_filter_enabled ? local.ENABLED_IPS : null

  _consistency_policy = [{
    consistency_level   = "Session"
  }]
  _geo_location       = [{
     location            = "${var.context.resource_location}"
     failover_priority   = 0
  }]
  _tags               = {
    CosmosAccountType       = "Non-Production"
    defaultExperience       = "Core (SQL)"
    hidden-cosmos-mmspecial = ""
    project_env             = local.project_env
  }
}

#cosmos db
resource "azurerm_cosmosdb_account" "cosmosacc_prevent_destroy" {
  count               = var.prevent_destroy ? 1 : 0
  name                = local.CALCULATED_NAME
  resource_group_name = var.context.resource_group_name
  location            = var.context.resource_location
  offer_type          = local._offer_type
  kind                = var.kind
  ip_range_filter     =  local._ip_range_filter
  is_virtual_network_filter_enabled = local._is_virtual_network_filter_enabled
  
  capabilities {
    name = var.capabilities_name
  }
  dynamic "virtual_network_rule" {
    for_each = var.is_virtual_network_filter_enabled && var.cosmossubnet_id != "" ? [var.cosmossubnet_id] : []
    content {
      id = var.cosmossubnet_id
    }
  }
  dynamic "consistency_policy" {
    for_each = local._consistency_policy
    content {
      consistency_level   = consistency_policy.value.consistency_level
    }    
  }
  dynamic "geo_location" {
    for_each = local._geo_location
    content {
      location            = geo_location.value.location
      failover_priority   = geo_location.value.failover_priority
    }    
  }
  tags = local._tags

  lifecycle {
    #we can't use a variable here
    # prevent_destroy = true
  }
}

resource "azurerm_cosmosdb_account" "cosmosacc" {
  count               = var.prevent_destroy ? 0 : 1
  name                = local.CALCULATED_NAME
  resource_group_name = var.context.resource_group_name
  location            = var.context.resource_location
  offer_type          = local._offer_type
  kind                = var.kind
  ip_range_filter     = local._ip_range_filter
  is_virtual_network_filter_enabled = local._is_virtual_network_filter_enabled
  
  dynamic "virtual_network_rule" {
    for_each = var.is_virtual_network_filter_enabled && var.cosmossubnet_id != "" ? [var.cosmossubnet_id] : []
    content {
      id = var.cosmossubnet_id
    }
  }
  dynamic "consistency_policy" {
    for_each = local._consistency_policy
    content {
      consistency_level   = consistency_policy.value.consistency_level
    }    
  }
  dynamic "geo_location" {
    for_each = local._geo_location
    content {
      location            = geo_location.value.location
      failover_priority   = geo_location.value.failover_priority
    }    
  }
  tags = local._tags

  lifecycle {
    #we can't use a variable here
    # prevent_destroy = false
  }
}
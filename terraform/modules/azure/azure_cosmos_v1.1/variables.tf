variable "context" {
  type = map(string)
  default = {
    project_name          = null
    environment           = null
    resource_group_name   = null
    resource_location     = null
  }
}

variable "name" {
  type = string
  description = "(Optional) Cosmos account name. When not specified, it will default to 'Project Name' + '-Environment' + '-cosmos'"
  default = ""
}
variable "capabilities_name" {
  type = string
  description = "(Optional) The capabilities which should be enabled for this Cosmos DB account. Possible values are EnableAggregationPipeline, EnableCassandra, EnableGremlin, EnableTable, MongoDBv3.4, and mongoEnableDocLevelTTL"
  default = "EnableAggregationPipeline"

}

variable "kind" {
  type = string
  description = "(Optional) Specifies the Kind of CosmosDB to create - possible values are GlobalDocumentDB and MongoDB. Defaults to GlobalDocumentDB. Changing this forces a new resource to be created."
  default = "GlobalDocumentDB"

}

variable "is_virtual_network_filter_enabled" {
  type = bool
  description = "(Optional) Whether virtual network filter is enabled. Defaults to false"
  default = false
}

variable "cosmossubnet_id" {
  type = string
  description = "(Optional) Subnet ID to associate this cosmos db account with.  Only valid when is_virtual_network_filter_enabled is 'true'"
  default = ""
}

variable "enable_azdatacenter_access" {
  type = bool
  description = "(Optional) Whether access from within azure datacenters is enabled. Only valid when is_virtual_network_filter_enabled is 'true'."
  default = true
}

variable "prevent_destroy" {
  type = bool
  description = "(Optional) Prevents resource destruction. Default to true for cosmos DB."
  default = true
}
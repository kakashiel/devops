output "account" {
  value = var.prevent_destroy ? azurerm_cosmosdb_account.cosmosacc_prevent_destroy[0] : azurerm_cosmosdb_account.cosmosacc[0]
}

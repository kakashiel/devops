resource "azurerm_storage_account" "func" {
  name                     = local.storage_account_name
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_kind             = "Storage"
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = merge(var.tags, map("environment", var.environment), map("release", var.release))
}


resource "azurerm_app_service_plan" "func" {
  name                = local.app_service_plan_name
  location            = var.location 
  resource_group_name = var.resource_group_name 
  kind                = "FunctionApp"

  sku {
    tier = "Standard"
    size = "S1"
  }

  tags = merge(var.tags, map("environment", var.environment), map("release", var.release))

}

resource "azurerm_application_insights" "insights" {
  count                = var.enable_application_insights ? 1 : 0
  name                 = local.function_app_name
  resource_group_name  = var.resource_group_name
  location             = var.location
  application_type     = "web"
}

resource "azurerm_function_app" "func" {
  name                      = local.function_app_name
  location                  = var.location 
  resource_group_name       = var.resource_group_name 
  app_service_plan_id       = azurerm_app_service_plan.func.id
  storage_connection_string = azurerm_storage_account.func.primary_connection_string

  https_only                = true
  version                   = var.function_version

  site_config {
    always_on               = var.always_on
    cors {
      allowed_origins = concat(local.CORS_DEFAULT_ORIGINS, var.cors_allowed_origins)
      support_credentials   = var.cors_support_credentials

    }
  }

  dynamic "connection_string" {
    for_each = var.connection_strings
    content {
      name = connection_string.value.name
      type = connection_string.value.type
      value = connection_string.value.value
    }
  }

  app_settings = merge(var.app_settings,
    {
      "APPINSIGHTS_INSTRUMENTATIONKEY" = var.enable_application_insights ? azurerm_application_insights.insights[0].instrumentation_key : null
      "APPLICATIONINSIGHTS_CONNECTION_STRING" = var.enable_application_insights ? "InstrumentationKey=${azurerm_application_insights.insights[0].instrumentation_key}" : null
      HASH = "${base64encode("${local.function_app_name}")}"
    }
  )
  


  tags = merge(var.tags, map("environment", var.environment), map("release", var.release))
   
   depends_on = [azurerm_application_insights.insights]

}
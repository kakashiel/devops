
output "storage_account_name" {
  description = "The name of the storage account created for the function app"
  value       = "${azurerm_storage_account.func.name}"
}

output "storage_account_connection_string" {
  description = "Connection string to the storage account created for the function app"
  value       = "${azurerm_storage_account.func.primary_connection_string}"
  sensitive   = true
}

output "storage_account_primary_access_key" {
  description = "Primary access key to the storage account created for the function app"
  value       = "${azurerm_storage_account.func.primary_access_key}"
  sensitive   = true
}
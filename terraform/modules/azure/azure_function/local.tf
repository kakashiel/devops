locals {
  storage_account_name    = "${replace("${lower(("${var.function_app_name}${var.environment}funcsta"))}", "/[^a-z0-9]/","")}"
  app_service_plan_name   = "${var.function_app_name}-${var.environment}-plan"
  function_app_name       = "${var.function_app_name}-${var.environment}-func"
  autoscale_settings_name = "${var.function_app_name}-${var.environment}-autoscale"
      __CORS_DEFAULT_PROD_ORIGINS = [
                                "https://functions-staging.azure.com",
                                "https://functions.azure.com",
                                "https://functions-next.azure.com"
                                ]
    __CORS_DEFAULT_DEV_ORIGINS = concat(["http://localhost:3000"], local.__CORS_DEFAULT_PROD_ORIGINS)
    CORS_DEFAULT_ORIGINS = var.environment == "prod" ? local.__CORS_DEFAULT_PROD_ORIGINS : local.__CORS_DEFAULT_DEV_ORIGINS
}
variable "resource_group_name" {
  description = "The name of the Resource Group the Function App will be contained in."
  default = "App"
}
variable "location" {
  description = "The name of the Resource Group the Function App will be contained in."
  default = "Australia East"
}

variable "function_app_name" {
  description = "The name for the function app. Without environment naming."
}

variable "function_version" {
  default     = "~3"
  description = "The runtime version the function app should have."
}

variable app_settings {
  description = "A map of app_settings to push to Azure Functions."
  type        = map(string)
  default = {}
}

variable connection_strings {
  description = "A list of connection string objects (name, type, value)"
  type        = list(object({
    name  = string
    type  = string
    value = string
  }))
  default = []
}

variable "enable_application_insights" {
  type = bool
  description = "Whether to enable application insights"
  default = false
}

variable always_on  {
  description = " Should the Function App be loaded at all times"
  default     = false
}

variable cors_support_credentials {
  description = "Are credentials supported?"
  default = false
}

variable cors_allowed_origins {
  description = "A map of cors_allowed_origins, put * for everything"
  type        = list(string)
  default = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map

  default = {}
}

variable "environment" {
  default     = "lab"
  description = "The environment where the infrastructure is deployed."
}

variable "release" {
  default     = ""
  description = "The release the deploy is based on."
}
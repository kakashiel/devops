
resource "azurerm_cosmosdb_mongo_database" "mongo" {
  name                = var.database_name
  resource_group_name = var.resource_group_name
  account_name        = var.azurerm_cosmosdb_account
}

resource "azurerm_cosmosdb_mongo_collection" "mongo" {
  count               = length(var.tables_name)
  name                = var.tables_name[count.index]
  resource_group_name = var.resource_group_name
  account_name        = var.azurerm_cosmosdb_account
  database_name       = azurerm_cosmosdb_mongo_database.mongo.name

  default_ttl_seconds = "777"
  shard_key           = "uniqueKey"
  throughput          = 400
}
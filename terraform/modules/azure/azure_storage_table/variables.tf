variable "resource_group_name" {
  description = "The name of the Resource Group the Function App will be contained in."
  default = "App"
}
variable "location" {
  description = "The name of the Resource Group the Function App will be contained in."
  default = "Australia East"
}

variable "environment" {
  default     = "lab"
  description = "The environment where the infrastructure is deployed."
}

variable "tables_name" {
  description = "Create IAM users with these names"
  type        = list(string)
  default     = ["table1", "table2"]
}
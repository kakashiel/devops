resource "azurerm_storage_account" "ddb" {
  name                     = local.storage_account_name
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  lifecycle {
    #we can't use a variable here
    prevent_destroy = true
  }
}

resource "azurerm_storage_table" "ddb" {
  count                = length(var.tables_name)
  name                 = var.tables_name[count.index]
  storage_account_name = azurerm_storage_account.ddb.name
  lifecycle {
    #we can't use a variable here
    prevent_destroy = true
  }
}
locals {
    project_env          = "${var.context.project_name}-${var.context.environment}"
    STORAGE_ACC_NAME     = "${replace("${lower(("${local.project_env}-fsta"))}", "/[^a-z0-9]/","")}"
    APP_SVC_NAME         = "${local.project_env}-fplan"
    FUNCTION_NAME        = "${local.project_env}-func"
    __CORS_DEFAULT_PROD_ORIGINS = [
                                "https://functions-staging.azure.com",
                                "https://functions.azure.com",
                                "https://functions-next.azure.com"
                                ]
    __CORS_DEFAULT_DEV_ORIGINS = concat(["http://localhost:3000"], local.__CORS_DEFAULT_PROD_ORIGINS)
    CORS_DEFAULT_ORIGINS = var.context.environment == "prod" ? local.__CORS_DEFAULT_PROD_ORIGINS : local.__CORS_DEFAULT_DEV_ORIGINS
}
  resource "azurerm_storage_account" "fsta" {  
  name                     = local.STORAGE_ACC_NAME
  resource_group_name      = var.context.resource_group_name
  location                 = var.context.resource_location
  account_kind             = "Storage"
  account_tier             = "Standard"
  account_replication_type = "LRS"
  tags = {
    project_env             = local.project_env
  }
}

resource "azurerm_app_service_plan" "fplan" {
  name                = local.APP_SVC_NAME
  resource_group_name = var.context.resource_group_name
  location            = var.context.resource_location
  kind                = "FunctionApp"

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
  tags = {
    project_env             = local.project_env
  }
}

resource "azurerm_application_insights" "insights" {
  count                = var.enable_application_insights ? 1 : 0
  name                 = local.FUNCTION_NAME
  resource_group_name  = var.context.resource_group_name
  location             = var.context.resource_location
  application_type     = "web"
}

resource "azurerm_function_app" "func" {
  name                      = local.FUNCTION_NAME
  resource_group_name       = var.context.resource_group_name
  location                  = var.context.resource_location
  app_service_plan_id       = azurerm_app_service_plan.fplan.id
  storage_connection_string = azurerm_storage_account.fsta.primary_connection_string
  app_settings              = merge({
    "APPINSIGHTS_INSTRUMENTATIONKEY" = var.enable_application_insights ? azurerm_application_insights.insights[0].instrumentation_key : null
    "APPLICATIONINSIGHTS_CONNECTION_STRING" = var.enable_application_insights ? "InstrumentationKey=${azurerm_application_insights.insights[0].instrumentation_key}" : null
    "FUNCTIONS_WORKER_RUNTIME"       = var.WorkerRuntime
    "WEBSITE_NODE_DEFAULT_VERSION"   = var.WorkerRuntime == "node" ? "~12" : null
    "HASH" = "${base64encode("${local.FUNCTION_NAME}")}"
  }, var.app_settings)
  version                   = "~3"
  https_only                = true
  
  site_config {
    cors {
      allowed_origins       = concat(local.CORS_DEFAULT_ORIGINS, var.cors_allowed_origins)
      support_credentials   = var.cors_support_credentials
    }
  }

  dynamic "connection_string" {
    for_each = var.connection_strings
    content {
      name = connection_string.value.name
      type = connection_string.value.type
      value = connection_string.value.value
    }
  }

  tags = {
    project_env             = local.project_env
  }
  depends_on = [azurerm_application_insights.insights]
}

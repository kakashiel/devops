variable "context" {
  type = map(string)
  default = {
    project_name          = null
    environment           = null
    resource_group_name   = null
    resource_location     = null
  }
}

variable "WorkerRuntime" {
  type = string
  description = "The function worker runtime language. Possible Values: ['node', 'dotnet']. It defaults to 'node'."
  default = "node"
}

variable "enable_application_insights" {
  type = bool
  description = "Whether to enable application insights"
  default = false
}

variable app_settings {
  description = "A map of app_settings to push to Azure Functions."
  type        = map(string)
  default = {}
}

variable cors_support_credentials {
  description = "Are credentials supported?"
  default = false
}

variable cors_allowed_origins {
  description = "A map of cors_allowed_origins, put * for everything"
  type        = list(string)
  default = []
}

variable connection_strings {
  description = "A list of connection string objects (name, type, value)"
  type        = list(object({
    name  = string
    type  = string
    value = string
  }))
  default = []
}

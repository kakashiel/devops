variable "context" {
  type = map(string)
  default = {
    project_name          = null
    environment           = null
    resource_group_name   = null
    resource_location     = null
  }
}

variable "signalr_name" {
  type = string
  description = "The function worker runtime language. Possible Values: ['node', 'dotnet']. It defaults to 'node'."
  default = null
}

variable cors_allowed_origins {
  description = "A map of cors_allowed_origins, put * for everything"
  type        = list(string)
  default = []
}

variable "features_value" {
  type = string
  default = "Default"
}

output "signalr_primary_connection_string" {
  value = "${azurerm_signalr_service.signalr.primary_connection_string}"
}

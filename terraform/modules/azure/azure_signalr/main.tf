resource "azurerm_signalr_service" "signalr" {
  name                = local.SIGNALR_NAME
  location            = var.context.resource_location
  resource_group_name = var.context.resource_group_name

  sku {
    name     = "Free_F1"
    capacity = 1
  }

  cors {
    allowed_origins = var.cors_allowed_origins
  }

  features {
    flag  = "ServiceMode"
    value = var.features_value
  }

  features {
    flag  = "EnableConnectivityLogs"
    value = "True"
  }

  tags = {
    project_env             = var.context.environment
  }
}

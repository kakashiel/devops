locals {
    project_env         = "${var.context.project_name}-${var.context.environment}"
    SIGNALR_NAME        = "${var.signalr_name == null ?   "signalr-${var.context.project_name}-${var.context.environment}" : var.signalr_name}"
}
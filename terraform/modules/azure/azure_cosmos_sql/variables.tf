variable "resource_group_name" {
  description = "The name of the Resource Group the Function App will be contained in."
  default = "App"
}
variable "location" {
  description = "The name of the Resource Group the Function App will be contained in."
  default = "Australia East"
}

variable "environment" {
  default     = "lab"
  description = "The environment where the infrastructure is deployed."
}

variable "azurerm_cosmosdb_account" {
  default     = ""
  description = "Link to azure cosomsdb account create before"
}

variable "database_name" {
  default     = ""
  description = "Database name"
}


variable "tables_name" {
  description = "Create IAM users with these names"
  type        = list(object({
    name  = string
    partition_key_path  = string
  }))
  default     = [    
    {
      name: "test",
      partition_key_path: "/_keytest"
    }
  ]
}
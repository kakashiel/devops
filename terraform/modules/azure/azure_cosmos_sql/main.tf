
resource "azurerm_cosmosdb_sql_database" "sql" {
  name                = var.database_name
  resource_group_name = var.resource_group_name
  account_name        = var.azurerm_cosmosdb_account
  throughput          = 400
}


resource "azurerm_cosmosdb_sql_container" "sql" {
  resource_group_name = var.resource_group_name
  account_name        = var.azurerm_cosmosdb_account
  database_name       = azurerm_cosmosdb_sql_database.sql.name

  count               = length(var.tables_name)
  name                = var.tables_name[count.index].name
  partition_key_path  = var.tables_name[count.index].partition_key_path
  throughput          = 400

}